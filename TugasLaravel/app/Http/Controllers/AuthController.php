<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        $fname = $request['fname'];
        $Lname = $request['Lname'];
        $male = $request['male'];
        $female = $request['female'];
        $other = $request['other'];
        $indonesia = $request['indonesia'];
        $amerika = $request['amerika'];
        $italia = $request['italia'];
        $bindonesia = $request['bindonesia'];
        $ingglish = $request['ingglish'];
        $bother = $request['bother'];
        $bio = $request['bio'];
        return view('welcome', 
        compact('fname','Lname','male','female','other','indonesia','amerika','italia','bindonesia','ingglish','bother','bio'));


    }
}
