<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home(){
        return view('halaman.index');
    }
    public function table(){
        return view('table.table');
    }
    public function datatable(){
        return view('table.datatabel');
    }
}
