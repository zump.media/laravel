<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/table', function(){
    return view('table.table');
    });
Route::get('/data-tables', function(){
    return view('table.datatabel');
});

Route::get('/cast/create', 'castController@create');
Route::post('/cast','castController@store' );
Route::get('/cast', 'castController@index');
Route::get('/cast/{cast_id}', 'castController@show');
Route::get('/cast/{cast_id}/edit', 'castController@edit');
Route::put('cast/{cast_id}', 'castController@update' );
Route::delete('cast/{cast_id}', 'castController@destroy' );

//tugas

Route::get('/game/create', 'GameController@create');
Route::post('/game','GameController@tambah' );
Route::get('/game', 'GameController@index');
Route::get('/game/{game_id}', 'GameController@show');
Route::get('/game/{game_id}/edit', 'GameController@edit');
Route::put('game/{game_id}', 'GameController@update' );
Route::delete('game/{game_id}', 'GameController@destroy' );


Route::get('/platform', 'GameController@platform');

Route::get('/genre/create', 'genreController@create');
Route::post('/genre','genreController@store' );
Route::get('/genre', 'genreController@index');
Route::get('/genre/{genre_id}', 'genreController@show');
Route::get('/genre/{genre_id}/edit', 'genreController@edit');
Route::put('genre/{genre_id}', 'genreController@update' );
Route::delete('genre/{genre_id}', 'genreController@destroy' );

Route::resource('film', 'filmController');