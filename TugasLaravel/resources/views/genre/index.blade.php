@extends('admin.master')

@section('judul')
    List Genre
@endsection

@section('content')
<a href="/genre/create" class="btn btn-primary mb-3">Tambah data</a>
<table class="table">
    
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">NAMA</th>
        
        <th scope="col">ACTION</th>
      </tr>
    </thead>

    <tbody>
     @forelse ($genre as $key=>$item)
         <tr>
             <td>{{$key + 1 }}</td>
             <td>{{$item->nama}}</td>
             
             <td>
                 
                 
                 <form action="/genre/{{$item->id}}" method="POST">
                    <a href="/genre/{{$item->id}}" class="btn btn-info btn sm">Detail</a>
                    <a href="/genre/{{$item->id}}/edit" class="btn btn-success btn sm">Update</a>
                    @method('delete')
                    @csrf
                    <input type="submit"  class="btn btn-danger btn sm" value="Delete">
                 </form>
                
             </td>
         </tr>
     @empty
         <tr>
             <td></td>
        </tr>
     @endforelse
    </tbody>
  </table>
  @endsection