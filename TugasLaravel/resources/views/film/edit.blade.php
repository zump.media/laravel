@extends('admin.master')

@section('judul')
    Halaman Edit Data Film
@endsection

@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label >Judul Film</label>
      <input type="text" name="judul" value="{{$film->judul}}" class="form-control" >
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Ringkasan</label>
        <textarea name="ringkasan" class="form-control" value="{{$film->ringkasan}}" cols="30" rows="10"></textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Tahun</label>
      <input type="INT" name="tahun" class="form-control" value="{{$film->tahun}}" >
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Poster Film</label>
      <input type="file" name="poster" class="form-control"  value="{{$film->poster}}">
    </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Genre Film</label>
      <select type="text" name="genre_id" class="form-control" id=""  >
          <option value="">--pilih genre--</option>
          @foreach($genre as $item)
            @if ($item->id === $film->genre_id)
                <option value="{{$item->id}}" selected> {{$item->nama}}</option>
            @endif
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
      </select>
    </div>
    @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection