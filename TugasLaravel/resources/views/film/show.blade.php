@extends('admin.master')

@section('judul')
    Halaman View Film {{$film->judul}}
@endsection

@section('content')

<h1>{{$film->judul}}</h1>
<img src="{{asset('poster/'. $film->poster)}}"  height="600px" width="600px" alt="...">
<p>{{$film->ringkasan}}</p>
@endsection