@extends('admin.master')

@section('judul')
    Halaman Tambah Film
@endsection

@section('content')
<div class="row">
    @forelse ($film as $item)
    <div class ="col-4 mt-2"> 
        <div class="card" style="width: 18rem;">
            <img src="{{asset('posterimg/'. $item->poster)}}" class="card-img-top" height="200px" width="200px" alt="...">
            <div class="card-body">
                <h5 class="card-title" >{{$item->judul}}</h5>
                <p class="card-text" >{{Str::limit($item->ringkasan, 50)}}</p>
                
                <form action="/film/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete " class="btn btn-danger sm">
                </form>
            </div>
        </div>
    </div> 
    @empty
        <h3>Belum ada data film</h3>
    @endforelse
</div>
@endsection