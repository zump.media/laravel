@extends('admin.master')

@section('judul')
    Halaman detail Cast {{$cast->nama}}
@endsection

@section('content')
<h3>{{$cast->nama}}</h3>
<h5>{{$cast->umur}} Tahun</h5>
<p>{{$cast->bio}}</p>
@endsection