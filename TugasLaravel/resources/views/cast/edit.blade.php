@extends('admin.master')

@section('judul')
    HAlaman Edit Cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label >Nama Cast</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="INT" name="umur" value="{{$cast->umur}}" class="form-control" >
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >bio</label>
        <textarea name="bio" class="form-control" value="{{$cast->bio}}" cols="30" rows="10"></textarea>
      </div>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection