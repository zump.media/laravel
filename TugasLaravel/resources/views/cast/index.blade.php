@extends('admin.master')

@section('judul')
    List Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah data</a>
<table class="table">
    
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">NAMA</th>
        <th scope="col">UMUR</th>
        <th scope="col">BIO</th>
        <th scope="col">ACTION</th>
      </tr>
    </thead>

    <tbody>
     @forelse ($cast as $key=>$item)
         <tr>
             <td>{{$key + 1 }}</td>
             <td>{{$item->nama}}</td>
             <td>{{$item->umur}}</td>
             <td>{{$item->bio}}</td>
             <td>
                 
                 
                 <form action="/cast/{{$item->id}}" method="POST">
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn sm">Update</a>
                    @method('delete')
                    @csrf
                    <input type="submit"  class="btn btn-danger btn sm" value="Delete">
                 </form>
                
             </td>
         </tr>
     @empty
         <tr>
             <td></td>
        </tr>
     @endforelse
    </tbody>
  </table>
  @endsection